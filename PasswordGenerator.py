import random
values = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
          "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
          "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6",
          "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H",
          "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
          "T", "U", "V", "W", "X", "Y", "Z", "-", "@", "!", "#",
          "$", "%", "&", "*", "+", ".", ";", "<", ">", "=", "~"]

print("Password Generator")
passwordCount = int(input("How many passwords would you like to generate?\n > "))
passwordLength = int(input("How long would you like the passwords to be?\n > "))
if isinstance(passwordCount, int):
    if isinstance(passwordLength, int):
        passwords = []
        for password in range(0, passwordCount):
            _password = []
            for character in range(0, passwordLength):
                _password.append(values[random.randint(0, len(values) - 1)])
                if len(_password) == passwordLength:
                    passwords.append(''.join(_password))
        if len(passwords) == passwordCount:
            print("Generated Passwords:")
            for password in passwords:
                print("• " + password)
