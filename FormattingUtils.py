# Terminal Formatting Utilities


# Colours
def colour_setup(colour: int):
    return "\33[" + str(colour) + "m"


class colour:
    white = colour_setup(30)
    red = colour_setup(31)
    green = colour_setup(32)
    yellow = colour_setup(33)
    blue = colour_setup(34)
    violet = colour_setup(35)
    cyan = colour_setup(36)
    grey = colour_setup(37)


# Strike
def strike(self: str):
    try:
        text = self
    except NameError as err:
        print("Error Occurred:\n Function: strike()\n Error: " + err)
    else:
        res = ""
        for i in range(0, len(text)):
            res = res + "\u0336" + text[i]
        return res
