# School Projects
A collection of school projects. Mainly Python orientated. 

## Examples

Various Python examples for me to refer to.

### GUI

Example GUI Python program using Tkinter

## Student Data System
This is a simple school project I made using Python which allows you to add a student to your virtual school by using the` create_student.py` script and then getting the data back using the `get_student_data.py` script.

The idea of this project was to teach me how to use File Structures and have a better understanding of Loops in Python.


## Microbit

Various school projects for the BBC Microbit.

### Conjoined

A simple game where you don't want to get hit by the attacker played on a 5 by 5 pixel grid.

## Directory Scanner

A simple application to scan directories.

## Password Generator

Generate any amount of passwords at any length.

## Formatting Utilities

Change the colour of text and strike through words with ease.
