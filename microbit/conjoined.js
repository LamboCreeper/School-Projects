let playerX = 0;
let playerXO = 0;
let playerY = 2;
let playerYO = 2;

basic.forever(() => {
    if (playerX > 4) {
        playerX = 0;
        led.unplot(playerXO, playerYO);
        led.plot(playerX, playerY);
    } else if (playerX < 0) {
        playerX = 4;
        led.unplot(playerXO, playerYO);
        led.plot(playerX, playerY);
    } else {
        led.unplot(playerXO, playerYO);
        led.plot(playerX, playerY);
    }

    playerXO = playerX;
    playerYO = playerY;
});

input.onButtonPressed(Button.A, () => {
    playerX = playerX - 1;
});

input.onButtonPressed(Button.B, () => {
    playerX = playerX + 1;
});
