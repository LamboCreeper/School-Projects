# Example GUI program using Tkinter

import urllib.request, tkinter as tk, json

class Application(tk.Frame):
    def __init__(self, master = None):
        super().__init__(master)
        self.pack()
        self.newWidget()

    def newWidget(self):
        self.test = tk.Button(self)
        self.test["text"] = "Button Text"
        self.test["command"] = self.run
        self.test.pack(side = "top")

        self.quit = tk.Button(self, text = "QUIT", fg = "red", command = root.destroy)
        self.quit.pack(side = "bottom")

    def run(self):
        print("Hey look this did something.")

root = tk.Tk()
app = Application(master = root)
app.mainloop()
