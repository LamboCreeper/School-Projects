# get_student_data.py

import linecache

f = open('student_data.txt', 'r')

student = input("Which student would you like information on?\n > ")

for num, line in enumerate(f, 1):
    if student in line:
        lines = f.readlines()
        print("Name | Age | School Year | Gender")
        print(linecache.getline('student_data.txt', num))
