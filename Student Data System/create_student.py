# create_student.py

student = ['not_defined', 'not_defined', 'not_defined', 'not_defined']

questions = ['What is your name?\n > ', 'How old are you?\n > ', 'What school year are you in?\n > ', 'What is your gender?\n > ']

f = open('student_data.txt', 'a')

for i in range(0, len(student)):
    if student[i] == 'not_defined':
        student[i] = input(questions[i])

f.write(student[0] + ' ' + student[1] + ' ' + student[2] + ' ' + student[3] + '\n')
f.close()
