# Student Data System

This is a simple school project I made using Python which allows you to add a student to your virtual school by using the `create_student.py` script.
This student can then be given it an age, school year and gender.
All of this data is stored in a `.txt` file and can be pulled from the `get_student_data.py` script.

The idea of this project was to teach me how to use File Structures and have a better understanding of Loops in Python.
