# Simple application to scan directories.

import os

def search():
    print("DirectoryScanner v1.0.0")
    _dir = input("Which directory should I search?\n > ")
    _ext = input("Which file extension should I look for?\n > ")
    try:
        _dir
    except NameError:
        print("Error: Directory not defined.")
        sys.exit()
    else:
        print("Finding results...")
        c = 0
        ct = 0
        if os.path.isdir(_dir):
            for file in os.listdir(_dir):
                name = os.path.splitext(os.path.join(_dir, file))[0]
                if _ext == '':  
                    print(" " + os.path.join(_dir, file))
                else:
                    if file.endswith(_ext):
                        print(" " + os.path.join(_dir, file))
                    else:
                        ct = ct + 1
            if c < ct:
                print("Error: No files matching that search criteria.")
            continue_search()
        else:
            print("Error: Unknown directory.")
            continue_search()

def continue_search():
    _b = input("Search another directory?\n > ")
    if _b.upper() == "TRUE" or _b.upper() == "YES" or _b.upper() == "Y":
        search()
    else:
        print("Thanks for using DirectoryScanner.")

search()
